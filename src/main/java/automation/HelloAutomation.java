package automation;

import java.io.File;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.testng.Assert;

public class HelloAutomation {

	public static void main(String[] args) {
		//safe way!
		
		String pathToGeckoDriver = "C:\\firefoxbrowsers\\geckodriver.exe";
		
		System.setProperty("webdriver.firefox.marionette", pathToGeckoDriver);
		
		String strPathToBinary = "C:\\firefoxbrowsers\\Firefox36\\firefox.exe";
		
		File pathToBinary = new File(strPathToBinary);
		FirefoxBinary ffBinary = new FirefoxBinary(pathToBinary);
		FirefoxProfile firefoxProfile = new FirefoxProfile();       
		WebDriver driver = new FirefoxDriver(ffBinary,firefoxProfile);
		
		driver.get("http://www.google.com.ar");
		
		WebElement textInput = driver.findElement(By.id("lst-ib"));
		
		textInput.sendKeys("Hello automation");
		
		WebElement searchButton = driver.findElement(By.id("sblsbb"));
		
		searchButton.click();
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		
		List<WebElement> resultsList = driver.findElements(By.xpath("//h2[contains(text(), 'Resultados de b�squeda')]/following-sibling::div//h3"));
		
		String shouldBeShown = "Automation en la Juti";
		boolean isShown = false;
		
		for (WebElement we : resultsList) {
			System.out.println(we.getText());
			if (we.getText().equals(shouldBeShown))
				isShown = true;
		}
		
		Assert.assertEquals(isShown, true);
		
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		driver.quit();
		
	}

}
