package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GoogleSearch {
	
	WebDriver driver;
	
	@FindBy(id="lst-ib")
	WebElement queryBox;
	
	@FindBy(id="sblsbb")
	WebElement searchButton;
	
	public GoogleSearch(WebDriver driver) {
		this.driver = driver;
		if (isPageLoaded() == true) {
			System.out.println("Page loaded succesfully");
		} else {
			throw new IllegalStateException("Can't load page");
		}
	}
	
	public boolean isPageLoaded() {
		WebDriverWait w = new WebDriverWait(driver, 5000);
		WebElement locator = w.until(ExpectedConditions.presenceOfElementLocated(getPageLocator()));
		return null != locator;
	}
	
	public By getPageLocator() {
		return By.id("lst-ib");
	}
	
	public void enterTextOnQueryBox(String s) {
		queryBox.sendKeys(s);
	}
	
	public void clickOnSearchButton() {
		searchButton.click();
	}
	
	public void searchSomething(String s) {
		enterTextOnQueryBox(s);
		searchButton.click();
	}
}
