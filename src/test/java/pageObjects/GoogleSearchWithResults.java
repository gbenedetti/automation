package pageObjects;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GoogleSearchWithResults extends GoogleSearch {

	@FindBy(xpath = "//h2[contains(text(), 'Resultados de b�squeda')]/following-sibling::div//h3")
	List<WebElement> resultsList;
	
	public GoogleSearchWithResults(WebDriver driver) {
		super(driver);
	}

	public By getPageLocator() {
		return By.xpath("//h2[contains(text(), 'Resultados de b�squeda')]/following-sibling::div//h3");
	}
	
	public List<String> listOfResults() {
		List<String> listToReturn = new ArrayList<String>();
		
		for (WebElement we : resultsList) {
			listToReturn.add(we.getText());
		}
		
		return listToReturn;
	}
}
