package automation;


import org.testng.Assert;
import org.testng.annotations.Test;

public class FirstTestNGClass {
  @Test
  public void testMethod() {
	  String test = "Mi primer test!";
	  Assert.assertEquals(test, "Mi primer test!");
  }
  
  @Test
  public void testMethod2() {
	  String test = "Mi primer failed!";
	  Assert.assertEquals(test, "Mi primer test!");
  }
  
  @Test
  public void testMethod3() {
	  int val = 2 + 2;
	  Assert.assertEquals(val, 3);
  }
  
  @Test
  public void testMethod4() {
	  int val = 2 + 2;
	  Assert.assertEquals(val, 4);
  }
}
