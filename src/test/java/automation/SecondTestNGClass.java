package automation;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class SecondTestNGClass {

	WebDriver driver;

	@Test
	public void testGoogle() {
		driver.get("http://www.google.com.ar");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		//do verifications
	}
	
	@Test
	public void testGmail() {
		driver.get("http://www.gmail.com");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		//do verifications
	}
	
	@Test
	public void testGDocs() {
		driver.get("http://www.docs.google.com");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		//do verifications
	}

	@BeforeClass
	public void beforeTest() {
		//before any test we set up de driver
		String pathToGeckoDriver = "C:\\firefoxbrowsers\\geckodriver.exe";
		
		System.setProperty("webdriver.firefox.marionette", pathToGeckoDriver);
		
		String strPathToBinary = "C:\\firefoxbrowsers\\Firefox36\\firefox.exe";
		
		File pathToBinary = new File(strPathToBinary);
		FirefoxBinary ffBinary = new FirefoxBinary(pathToBinary);
		FirefoxProfile firefoxProfile = new FirefoxProfile();       
		driver = new FirefoxDriver(ffBinary,firefoxProfile);
	}
	
	@AfterTest
	public void afterTesting() {
		driver.quit();
	}
	

}
