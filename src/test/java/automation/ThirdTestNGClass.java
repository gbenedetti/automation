package automation;

import java.io.File;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pageObjects.GoogleSearch;
import pageObjects.GoogleSearchWithResults;

public class ThirdTestNGClass {
	
	WebDriver driver;
	
	@Test
	public void testGoogleWithPageObject() {
		driver.get("http://www.google.com.ar");
		GoogleSearch gs = PageFactory.initElements(driver, GoogleSearch.class);
		gs.searchSomething("TestNG Tutorials");
	}
	
	@Test
	public void testGoogleWithPageObjectAndResults() {
		GoogleSearchWithResults gswr = PageFactory.initElements(driver, GoogleSearchWithResults.class);
		List<String> ls = gswr.listOfResults();
		for (String s : ls) {
			System.out.println(s);
		}
	}

	@BeforeTest
	public void beforeTest() {
		// before any test we set up de driver
		String pathToGeckoDriver = "C:\\firefoxbrowsers\\geckodriver.exe";

		System.setProperty("webdriver.firefox.marionette", pathToGeckoDriver);

		String strPathToBinary = "C:\\firefoxbrowsers\\Firefox36\\firefox.exe";

		File pathToBinary = new File(strPathToBinary);
		FirefoxBinary ffBinary = new FirefoxBinary(pathToBinary);
		FirefoxProfile firefoxProfile = new FirefoxProfile();
		driver = new FirefoxDriver(ffBinary, firefoxProfile);
	}

	@AfterTest
	public void afterTest() {
	}

}
